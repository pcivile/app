package com.example.rossi.protezionecivile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class EmergenzaFragment extends Fragment {

    JSONObject emergency = null;
    boolean executed = false;
    private View rootView;
    LastEmergency lastEmergency;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //setta il titolo e prova a vedere se c'é una view in cache

        Objects.requireNonNull(getActivity()).setTitle("Emergenze");
        return getPersistentView(inflater, container, savedInstanceState, R.layout.fragment_emergenza);

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);


        Animation animFadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
        FrameLayout baseLayout = getActivity().findViewById(R.id.emergenza_base);
        baseLayout.startAnimation(animFadeIn);

        JSONObject lastEmergencyJSON = null;
        if(lastEmergency != null)
            lastEmergencyJSON = lastEmergency.getResponse();
        lastEmergency = new LastEmergency(this);
        if(emergency != lastEmergencyJSON){
            executed = true;
            emergency = lastEmergencyJSON;
        }
        lastEmergency.setExecuted(executed);
        lastEmergency.execute(getContext());



    }


    public View getPersistentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, int layout){
        if(rootView == null)
            rootView = inflater.inflate(layout, null);
        else ((ViewGroup)rootView).removeView(rootView);
        return rootView;
    }

    public boolean getExecuted(){
        return executed;
    }
}


class LastEmergency extends AsyncTask<Context, Void, JSONObject> {

    private boolean executed = false;
    private JSONObject emergency = null;
    private Fragment mContext;

    void setExecuted(boolean executed){
        this.executed = executed;
    }

    LastEmergency(Fragment fragment){
        this.mContext = fragment;
    }
    @Override

    protected JSONObject doInBackground(final Context... contexts) {

        String url = "https://pcivile1.altervista.org/RESTAPI/emergenza.php";

        SharedPreferences prefs = contexts[0].getSharedPreferences("login", Context.MODE_PRIVATE);

        Map<String, String> params = new HashMap<>();

        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "0")));

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                emergency = response;
                Log.i("JSONOBJECT", emergency.toString());
                onPostExecute(emergency);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        //attivo la disponibilita' al login
        url = "https://pcivile1.altervista.org/RESTAPI/cambiaDisponibile.php";

        params = new HashMap<>();

        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "")));
        params.put("disponibile", String.valueOf(1));

        parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest2 = new JsonObjectRequest(Request.Method.POST, url, parameters, null, null);

        Volley.newRequestQueue(contexts[0]).add(jsonRequest);
        Volley.newRequestQueue(contexts[0]).add(jsonRequest2);


        return emergency;
    }

    @Override
    protected void onPostExecute(final JSONObject jsonObject) {
        if(jsonObject != null){
            try {
                LinearLayout l = Objects.requireNonNull(mContext.getView()).findViewById(R.id.emergenza_layout);
                l.removeAllViews();

                if(jsonObject.getBoolean("success")) {

                    List<String> labels = Arrays.asList("Localita':", "Tipo:", "Codice:");
                    List<String> values = Arrays.asList(jsonObject.getString("Localita"), jsonObject.getString("Tipo"), jsonObject.getString("Grado"));



                    if (jsonObject.has("Latitudine") && jsonObject.has("Longitudine")) {
                        if(!(jsonObject.isNull("Latitudine") && jsonObject.isNull("Longitudine"))) {

                            //istanza mappa
                            SmallMapFragment m1;
                            //altezza della mappa responsiva
                            Display display = Objects.requireNonNull(mContext.getActivity()).getWindowManager().getDefaultDisplay();
                            int dHeight = display.getHeight(); dHeight = dHeight/2;
                            FrameLayout f = mContext.getActivity().findViewById(R.id.map);
                            f.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dHeight));
                            m1 = SmallMapFragment.newInstance(Double.parseDouble((String) jsonObject.get("Latitudine")), Double.parseDouble((String) jsonObject.get("Longitudine")));
                            mContext.getChildFragmentManager().beginTransaction().add(R.id.map, m1).commit();
                        }
                    }

                    for(int i=0; i<labels.size(); i++){

                        if(i > 0) {
                            View line = new View(mContext.getContext());
                            line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 4));
                            line.setBackgroundColor(Color.rgb(211, 211, 211));
                            l.addView(line);
                        }


                        //creazione del layout del fragment emergenza

                        TextView label = new TextView(mContext.getContext());               //label
                        TextView value = new TextView(mContext.getContext());               //oggetto della risposta
                        LinearLayout layout = new LinearLayout(mContext.getContext());      //layout contenente label e string


                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        layoutParams.setMargins(40,20,40,20);


                        layout.setLayoutParams(layoutParams);
                        layout.setWeightSum(2);
                        layout.setOrientation(LinearLayout.HORIZONTAL);


                        label.setText(labels.get(i));
                        label.setTypeface(null, Typeface.BOLD);
                        label.setTextSize(25);
                        label.setLayoutParams(new LinearLayout.LayoutParams( 0,LinearLayout.LayoutParams.WRAP_CONTENT, 1));


                        value.setText(values.get(i));
                        value.setTextSize(25);
                        value.setLayoutParams(new LinearLayout.LayoutParams( 0,LinearLayout.LayoutParams.WRAP_CONTENT,  1));


                        layout.addView(label); layout.addView(value);
                        l.addView(layout);
                    }
                    if(jsonObject.has("accettato")){
                        if(jsonObject.getInt("accettato") == 2){
                            final CardView cardView = mContext.getView().findViewById(R.id.accept_emergency);
                            cardView.setVisibility(View.VISIBLE);

                            Button acceptButton = mContext.getView().findViewById(R.id.accept_button);
                            Button refuseButton = mContext.getView().findViewById(R.id.refuse_button);

                            acceptButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    String url = "https://pcivile1.altervista.org/RESTAPI/rispostaEmergenza.php";

                                    SharedPreferences prefs = Objects.requireNonNull(mContext.getContext()).getSharedPreferences("login", Context.MODE_PRIVATE);

                                    Map<String, String> params = new HashMap<>();

                                    params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "0")));
                                    Log.i("id_utente", prefs.getString("id_utente", "0"));
                                    params.put("accettato", "1");
                                    try {
                                        params.put("id_emergenza", jsonObject.getString("id_emergenza"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    JSONObject parameters = new JSONObject(params);
                                    JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters,null,null);
                                    Volley.newRequestQueue(mContext.getContext()).add(jsonRequest);
                                    Animation animFadeOut = AnimationUtils.loadAnimation(mContext.getContext(),R.anim.fadeout);
                                    cardView.startAnimation(animFadeOut);
                                    cardView.setVisibility(View.GONE);
                                }
                            });

                            refuseButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    String url = "https://pcivile1.altervista.org/RESTAPI/rispostaEmergenza.php";

                                    SharedPreferences prefs = Objects.requireNonNull(mContext.getContext()).getSharedPreferences("login", Context.MODE_PRIVATE);

                                    Map<String, String> params = new HashMap<>();

                                    params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "0")));
                                    params.put("accettato", String.valueOf(0));

                                    try {
                                        params.put("id_emergenza", jsonObject.getString("id_emergenza"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    JSONObject parameters = new JSONObject(params);

                                    Log.i("JSONOBJECT", parameters.toString());
                                    JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, null , null);
                                    Volley.newRequestQueue(mContext.getContext()).add(jsonRequest);
                                    Animation animFadeOut = AnimationUtils.loadAnimation(mContext.getContext(),R.anim.fadeout);
                                    cardView.startAnimation(animFadeOut);
                                    cardView.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                }
                else {
                    TextView t1 = new TextView(mContext.getContext());
                    FrameLayout map = mContext.getView().findViewById(R.id.map);
                    map.removeAllViews();
                    map.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0));
                    t1.setText(mContext.getString(R.string.no_emergencies));
                    t1.setTextSize(30);
                    t1.setGravity(Gravity.CENTER);
                    t1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    l.addView(t1);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("JSONOBJECT", jsonObject.toString());
        }
    }

    JSONObject getResponse(){
        return emergency;
    }
}