package com.example.rossi.protezionecivile;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class AnagraficaFragment extends Fragment {

    public boolean initialized = false;
    private View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //setta il titolo e prova a vedere se c'é una view in cache

        Objects.requireNonNull(getActivity()).setTitle("Anagrafica");
        return getPersistentView(inflater, container, savedInstanceState, R.layout.fragment_anagrafica);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences prefs = getContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        boolean darkTheme = prefs.getBoolean("theme", false);

        if(!darkTheme){
            CardView card = Objects.requireNonNull(getActivity()).findViewById(R.id.card_anagrafica);
            card.setCardBackgroundColor(Color.WHITE);
        }

        Animation animFadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
        FrameLayout baseLayout = getActivity().findViewById(R.id.anagrafica_base);
        baseLayout.startAnimation(animFadeIn);

        if(!initialized){
            initialized = true;

            String id = prefs.getString("id_utente", "non esiste");

            //qui va il codice per l' inizializzazione
            anagrafica(id);
        }
    }

    public View getPersistentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, int layout){
        if(rootView == null)
            rootView = inflater.inflate(layout, null);
        else ((ViewGroup)rootView).removeView(rootView);
        return rootView;
    }

    public void anagrafica(String id_utente)
    {
        String url = "https://pcivile1.altervista.org/RESTAPI/anagrafica.php";
        SharedPreferences prefs = getContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        Map<String, String> params = new HashMap<>();


        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "0")));

        JSONObject parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                LinearLayout l = getView().findViewById(R.id.anagrafica_layout);
                try {
                    if (response.getBoolean("success")) {

                        TextView header = getActivity().findViewById(R.id.hello);

                        if(response.getInt("Caposquadra") == 1)
                            header.setText("Benvenuto caposquadra");
                        else
                            header.setText("Benvenuto volontario");

                        List<String> labels = Arrays.asList("Localita':", "Nome:", "Cognome:", "Disponibilita':");
                        List<String> values = Arrays.asList(response.getString("Localita"), response.getString("Nome"), response.getString("Cognome"), response.getString("Disponibilita"));

                        Animation animFadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
                        l.startAnimation(animFadeIn);

                        for(int i=0; i<labels.size(); i++) {

                            View line = new View(getContext());
                            line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 4));
                            line.setBackgroundColor(Color.rgb(211, 211, 211));
                            l.addView(line);

                            LinearLayout layout = new LinearLayout(getContext());           //layout contenente label e string
                            TextView label = new TextView(getContext());                    //label


                            //creazione del layout del fragment anagrafica
                            if (!labels.get(i).equals("Disponibilita':")) {


                                TextView value = null;
                                value = new TextView(getContext());               //oggetto della risposta



                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.setMargins(40, 20, 40, 20);


                                layout.setLayoutParams(layoutParams);
                                layout.setWeightSum(2);
                                layout.setOrientation(LinearLayout.HORIZONTAL);


                                label.setText(labels.get(i));
                                label.setTypeface(null, Typeface.BOLD);
                                label.setTextSize(25);
                                label.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));

                                value.setText(values.get(i));
                                value.setTextSize(25);
                                value.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));


                                layout.addView(label);
                                layout.addView(value);
                            }
                            else {

                                ImageView value = new ImageView(getContext());

                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.setMargins(40, 20, 40, 20);


                                layout.setLayoutParams(layoutParams);
                                layout.setOrientation(LinearLayout.HORIZONTAL);


                                label.setText(labels.get(i));
                                label.setTypeface(null, Typeface.BOLD);
                                label.setTextSize(25);
                                label.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));


                                Drawable d;

                                if(values.get(i).equals("1"))
                                    d = getResources().getDrawable(R.drawable.green);
                                else
                                    d = getResources().getDrawable(R.drawable.red);


                                Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 35, true));


                                value.setImageDrawable(d);
                                value.setId(R.id.image_id);

                                layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                //layoutParams.gravity = Gravity.LEFT;
                                value.setLayoutParams(layoutParams);
                                layout.addView(label);
                                layout.addView(value);
                            }


                            l.addView(layout);
                        }
                    }
                    else {
                        TextView t1 = new TextView(getContext());
                        t1.setText(getString(R.string.error));
                        t1.setTextSize(30);
                        t1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        l.addView(t1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        Volley.newRequestQueue(getContext()).add(jsonRequest);
    }

    @Nullable
    public static Drawable LoadImageFromWeb(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }
}
