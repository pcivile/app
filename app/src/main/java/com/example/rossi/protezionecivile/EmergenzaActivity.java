package com.example.rossi.protezionecivile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EmergenzaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        final boolean darkTheme = prefs.getBoolean("theme", false);

        if(darkTheme)
            setTheme(R.style.AppThemeDark);
        else setTheme(R.style.AppThemeLight);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergenza);


        Animation animFadeIn = AnimationUtils.loadAnimation(this,R.anim.fadein);
        FrameLayout baseLayout = findViewById(R.id.emergenza_base_activity);
        baseLayout.startAnimation(animFadeIn);

        Intent i = getIntent();
        String id_emergenza = i.getStringExtra("ID_Emergenza");

        Toolbar mToolbar = findViewById(R.id.toolbar2);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setTitle("Emergenza Passata");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        this.setTitle("Emergenze");

        String url = "https://pcivile1.altervista.org/RESTAPI/emergenza.php";

        final Map<String, String> params = new HashMap<>();

        params.put("id_emergenza", id_emergenza);


        JSONObject parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            LinearLayout l = findViewById(R.id.emergenza_layout_activity);
            try {
                if (response.getBoolean("success")) {

                    List<String> labels = Arrays.asList("Localita':", "Tipo:", "Codice:", "Data:");
                    List<String> values = Arrays.asList(response.getString("Localita"), response.getString("Tipo"), response.getString("Grado"), response.getString("Data"));


                    if (response.has("Latitudine") && response.has("Longitudine")) {
                        if(!(response.isNull("Latitudine") && response.isNull("Longitudine"))) {

                            //istanza mappa
                            SmallMapFragment m1;
                            //altezza della mappa responsiva
                            Display display = getWindowManager().getDefaultDisplay();
                            int dHeight = display.getHeight();
                            dHeight = dHeight / 2;
                            FrameLayout f = findViewById(R.id.map_activity);
                            f.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dHeight));

                            m1 = SmallMapFragment.newInstance(Double.parseDouble((String) response.get("Latitudine")), Double.parseDouble((String) response.get("Longitudine")));
                            getSupportFragmentManager().beginTransaction().replace(R.id.map_activity, m1).commitAllowingStateLoss();
                        }
                    }

                    Animation animFadeIn = AnimationUtils.loadAnimation(EmergenzaActivity.this,R.anim.fadein);
                    l.startAnimation(animFadeIn);

                    for(int i=0; i<labels.size(); i++){

                        if(i > 0) {
                            View line = new View(EmergenzaActivity.this);
                            line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 4));
                            line.setBackgroundColor(Color.rgb(211, 211, 211));
                            l.addView(line);
                        }


                        //creazione del layout del fragment emergenza

                        TextView label = new TextView(EmergenzaActivity.this);               //label
                        TextView value = new TextView(EmergenzaActivity.this);               //oggetto della risposta
                        LinearLayout layout = new LinearLayout(EmergenzaActivity.this);      //layout contenente label e string


                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(40,20,40,20);


                        layout.setLayoutParams(layoutParams);
                        layout.setWeightSum(2);
                        layout.setOrientation(LinearLayout.HORIZONTAL);


                        label.setText(labels.get(i));
                        label.setTypeface(null, Typeface.BOLD);
                        label.setTextSize(25);
                        label.setLayoutParams(new LinearLayout.LayoutParams( 0,LinearLayout.LayoutParams.WRAP_CONTENT, 1));

                        if(labels.get(i).equals("Data")){
                            int timestamp = Integer.parseInt(values.get(i));
                            Calendar cal = Calendar.getInstance(Locale.ITALY);
                            cal.setTimeInMillis(timestamp);
                            String labelDate = DateFormat.format("dd-MM-yyy", cal).toString();
                            value.setText(labelDate);
                        }
                        else value.setText(values.get(i));
                        value.setTextSize(25);
                        value.setLayoutParams(new LinearLayout.LayoutParams( 0,LinearLayout.LayoutParams.WRAP_CONTENT,  1));


                        layout.addView(label); layout.addView(value);
                        l.addView(layout);

                    }
                }
                else {
                    TextView t1 = new TextView(EmergenzaActivity.this);
                    t1.setText(getString(R.string.error));
                    t1.setTextSize(30);
                    t1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    l.addView(t1);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
        }
        });

        Volley.newRequestQueue(this).add(jsonRequest);

    }
}
