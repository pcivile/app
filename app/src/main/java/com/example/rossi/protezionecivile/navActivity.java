package com.example.rossi.protezionecivile;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class navActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<Integer> fragment_history = new ArrayList<>();

    private static final String TAG_EMERGENCY = "fragment_one";
    private static final String TAG_INTERVENTI = "fragment_two";
    private static final String TAG_DATI = "fragment_three";

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int INITIAL_REQUEST=1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        boolean darkTheme = prefs.getBoolean("theme", false);

        if(darkTheme)
            setTheme(R.style.AppThemeDark);
        else setTheme(R.style.AppThemeLight_NoActionBar);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checkAvailability();
        Switch themeSwitch = findViewById(R.id.theme_switch);
        themeSwitch.setChecked(darkTheme);
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        Fragment fragment = null;

        assert fragment != null;
        transaction.replace(R.id.content, new EmergenzaFragment(), TAG_EMERGENCY);
        fragment_history.add(R.id.emergency);
        transaction.commit();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                checkAvailability();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }else
            LocationJobService.schedule(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == INITIAL_REQUEST){
            Log.i("LocationJobService", "STARTING");
            LocationJobService.schedule(this);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onMenuClick(MenuItem item) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        prefs.edit().remove("user").apply();
        prefs.edit().remove("pass").apply();
        prefs.edit().remove("id_utente").apply();
        String url = "https://pcivile1.altervista.org/RESTAPI/cambiaDisponibile.php";

        final Map<String, String> params = new HashMap<>();

        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "")));
        params.put("disponibile", String.valueOf(0));

        JSONObject parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, null, null);
        Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);

        Intent i = new Intent(getApplicationContext(), loginActivity.class);

        LocationJobService.cancel(this);

        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getSupportFragmentManager().getBackStackEntryCount() != 0) {
                fragment_history.remove(fragment_history.size() - 1);
                getSupportFragmentManager().popBackStack();
                NavigationView navigationView = findViewById(R.id.nav_view);

                switch (fragment_history.get(fragment_history.size()-1)){
                    case(R.id.emergency) :
                        navigationView.getMenu().getItem(0).setChecked(true);
                        break;

                    case(R.id.interventi) :
                        navigationView.getMenu().getItem(1).setChecked(true);
                        break;

                    case(R.id.dati_user) :
                        navigationView.getMenu().getItem(2).setChecked(true);
                        break;
                }
            }
            else super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String tag = null;

        DrawerLayout drawer;
        drawer = findViewById(R.id.drawer_layout);


        if(id != fragment_history.get(fragment_history.size()-1)){
            fragment_history.add(id);
            Fragment fragment = null;
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Log.i("ID1:", String.valueOf(id));

            switch(id) {

                case(R.id.emergency) :
                    tag = TAG_EMERGENCY;
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                    if(fragment == null)
                        fragment = new EmergenzaFragment();
                    break;

                case(R.id.interventi) :
                    tag = TAG_INTERVENTI;
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                    if(fragment == null)
                        fragment = new InterventiFragment();
                    break;

                case(R.id.dati_user) :
                    tag = TAG_DATI;
                    fragment = getSupportFragmentManager().findFragmentByTag(tag);
                    if(fragment == null)
                        fragment = new AnagraficaFragment();
                    break;
            }
            try {

                assert fragment != null;
                transaction.replace(R.id.content, fragment, tag);
                transaction.addToBackStack(null);
                transaction.commit();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void toggleTheme(View v){

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        boolean themeSwitch = prefs.getBoolean("theme", false);
        prefs.edit().putBoolean("theme", !themeSwitch).apply();


        Intent intent=new Intent();
        intent.setClass(this, this.getClass());
        this.startActivity(intent);
        this.finish();

    }

    //toggle della disponibilita'
    public void toggleAvailability(View v) {
        Switch s = findViewById(R.id.available_switch);

        int i;
        if(s.isChecked()){
            i = 1;
            s.setChecked(true);
        }
        else{
            i = 0;
            s.setChecked(false);
        }
        Log.i("SWITCH",String.valueOf(i));

        String url = "https://pcivile1.altervista.org/RESTAPI/cambiaDisponibile.php";

        final Map<String, String> params = new HashMap<>();

        SharedPreferences prefs = getSharedPreferences("login", Context.MODE_PRIVATE);
        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "")));
        params.put("disponibile", String.valueOf(i));

        JSONObject parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, null, null);
        Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);

        if(fragment_history.contains(R.id.dati_user)){
            ImageView img = findViewById(R.id.image_id);
            if(img != null) {
                Drawable d;

                if (i == 1)
                    d = getResources().getDrawable(R.drawable.green);
                else
                    d = getResources().getDrawable(R.drawable.red);

                Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 35, true));

                img.setImageDrawable(d);
            }
        }
    }

    //controlla all'avvio la disponibilita' del volontario
    public void checkAvailability() {

        Switch s = findViewById(R.id.available_switch);
        String url = "https://pcivile1.altervista.org/RESTAPI/disponibile.php";

        final Map<String, String> params = new HashMap<>();

        SharedPreferences prefs = getSharedPreferences("login", Context.MODE_PRIVATE);
        params.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "")));
        if(s != null)
            s.setEnabled(false);

        JSONObject parameters = new JSONObject(params);
        final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getBoolean("success"))
                    {
                        Switch s = findViewById(R.id.available_switch);
                        if(s != null){
                            if(response.getInt("available") == 1)
                                s.setChecked(true);
                            else s.setChecked(false);

                            s.setEnabled(true);
                        }
                        //Log.i("AVAILABLE", String.valueOf(response.getBoolean("available")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);
    }

    private boolean canAccessLocation() {
        return(this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }
}

