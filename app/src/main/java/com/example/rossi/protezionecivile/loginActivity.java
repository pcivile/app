package com.example.rossi.protezionecivile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class loginActivity extends AppCompatActivity implements View.OnClickListener{

    Button login;
    TextView user;
    TextView pass;
    ProgressDialog loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences prefs = this.getSharedPreferences("login", Context.MODE_PRIVATE);
        boolean darkTheme = prefs.getBoolean("theme", false);
        prefs.edit().putBoolean("theme", darkTheme);

        if(darkTheme)
            this.setTheme(R.style.AppThemeDark);
        else setTheme(R.style.AppThemeLight);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //prendo le view
        login = findViewById(R.id.send);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.pass);
        loader = new ProgressDialog(this);
        loader.setIndeterminate(true);
        loader.setMessage("Accesso...");


        //onclick listener sul bottone per iniziare il login
        login.setOnClickListener(this);

        //se esistono user e pass nelle shared preference usa quelle
        String userShared = prefs.getString("user", null);
        String passShared = prefs.getString("pass", null);
        if(userShared != null && passShared != null){
            user.setText(userShared);
            pass.setText(passShared);
            login.setClickable(false);
            startLogin();
        }



    }

    @Override
    public void onClick(View v) {
        startLogin();
    }


    public void startLogin(){

        loader.show();

        FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {

                    if(!task.isSuccessful()){
                        Log.w("TOKEN", "getInstanceId failed");
                        return;
                    }

                    //parametri della volleyrequest

                    String url = "https://pcivile1.altervista.org/RESTAPI/auth.php";

                    Map<String, String> params = new HashMap<>();

                    params.put("username", user.getText().toString());
                    params.put("password", pass.getText().toString());
                    params.put("token", Objects.requireNonNull(task.getResult()).getToken());


                    JSONObject parameters = new JSONObject(params);
                    JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if(response.getBoolean("success"))
                                {
                                    //in caso di successo avvio la navActivity e memorizzo le credenziali
                                    Intent intent = new Intent(getApplicationContext(), navActivity.class);
                                    intent.putExtra("USER", user.getText().toString());
                                    intent.putExtra("ID", response.getString("id"));

                                    SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                                    prefs.edit().putString("user", user.getText().toString()).apply();
                                    prefs.edit().putString("pass", pass.getText().toString()).apply();
                                    prefs.edit().putString("id_utente", response.getString("id")).apply();
                                    startActivity(intent);
                                    loader.dismiss();

                                    finish();
                                }
                                else{
                                    Toast.makeText(getApplicationContext(), "Nome utente o password errata!", Toast.LENGTH_LONG).show();
                                    loader.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

                    Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);
                }
            });
    }
}
