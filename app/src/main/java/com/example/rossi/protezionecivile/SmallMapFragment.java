package com.example.rossi.protezionecivile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

/*
        classe per la creazione del fragment contenente
        una mappa, date le coordinate
*/

@SuppressLint("ValidFragment")
public class SmallMapFragment extends android.support.v4.app.Fragment implements OnMapReadyCallback{

    private Double lat;
    private Double lng;
    private LatLng latLng;
    private GoogleMap mMap;
    private Marker marker;

    public SmallMapFragment(){

    }

    public static SmallMapFragment newInstance(Double lat, Double lng) {

        Bundle args = new Bundle();

        args.putDouble("lat", lat);
        args.putDouble("lng", lng);
        SmallMapFragment fragment = new SmallMapFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        setUpMapIfNeeded();
        return view;
    }



    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.small_map);
            if(mapFrag != null){ mapFrag.getMapAsync(this);}
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }


    private void setUpMap() {

        if (marker != null) {
            marker.remove();
        }
        lat = getArguments().getDouble("lat");
        lng = getArguments().getDouble("lng");
        latLng = new LatLng(lat, lng);
        MarkerOptions markerOptions = new MarkerOptions().
                position(latLng).
                icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier("marker","drawable", Objects.requireNonNull(getActivity()).getPackageName()) ), 50, 90, false)));

        marker = mMap.addMarker(markerOptions);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ lat + "," + lng);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if(mapIntent.resolveActivity(getActivity().getPackageManager()) != null)
                    getContext().startActivity(mapIntent);
                return false;
            }
        });

        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);

        SharedPreferences prefs = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        boolean darkTheme = prefs.getBoolean("theme", false);
        if(darkTheme) {
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(Objects.requireNonNull(getContext()), R.raw.mapstyle_night);
            mMap.setMapStyle(style);
        }

        mMap.getUiSettings().setScrollGesturesEnabled(false);

    }
}
