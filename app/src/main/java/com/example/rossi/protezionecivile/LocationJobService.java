package com.example.rossi.protezionecivile;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LocationJobService extends JobService implements LocationListener {


    private static final int PERIOD = 1000 * 60 * 30;
    private static final int JOB_ID = 1337;

    private FusedLocationProviderClient fusedLocationClient;

    @SuppressLint("MissingPermission")

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.i("LocationJobService", "STARTED");

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getMainExecutor(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            updateLocation(location);
                        }
                    });
        }
        else{
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }


        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        Log.i("LocationJobService", "STOPPED");

        return false;
    }

    public static void schedule(Context context){
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(context, LocationJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, componentName);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setPeriodic(PERIOD);
        jobScheduler.schedule(builder.build());

        Log.i("LocationJobService", "SCHEDULED");
    }

    public static void cancel(Context context){
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }

    private void updateLocation(Location location){
        if (location != null) {
            SharedPreferences prefs = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);

            String url = "https://pcivile1.altervista.org/RESTAPI/aggiornaPosizione.php";

            final Map<String, String> requestParams = new HashMap<>();

            requestParams.put("id_utente", Objects.requireNonNull(prefs.getString("id_utente", "")));
            requestParams.put("lat", String.valueOf(location.getLatitude()));
            requestParams.put("lng", String.valueOf(location.getLongitude()));

            JSONObject parameters = new JSONObject(requestParams);

            Log.i("LocationJobService", parameters.toString());

            final JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, null, null);
            Volley.newRequestQueue(getApplicationContext()).add(jsonRequest);

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("LocationJobService","Status Changed");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("LocationJobService","Enabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("LocationJobService","Disabled");
    }
}
