package com.example.rossi.protezionecivile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class InterventiFragment extends Fragment {

    public boolean initialized = false;
    private View rootView;

    ArrayAdapter<EmergenzaList> adapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Animation animFadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
        ConstraintLayout baseLayout = getActivity().findViewById(R.id.interventi_base);
        baseLayout.startAnimation(animFadeIn);

        if(!initialized) {
            initialized = true;
            adapter = new ArrayAdapter<>(Objects.requireNonNull(this.getContext()), R.layout.arrayadapter_interventi);
            Context con = Objects.requireNonNull(getActivity()).getApplicationContext();
            SharedPreferences prefs = con.getSharedPreferences("login", Context.MODE_PRIVATE);
            String id = prefs.getString("id_utente", "non esiste");
            caricaInterventi(id);

            lista =  Objects.requireNonNull(getView()).findViewById(R.id.emergenze_list);
            lista.setAdapter(adapter);
        }
    }

    ListView lista;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //setta il titolo e prova a vedere se c'é una view in cache

        Objects.requireNonNull(getActivity()).setTitle("Interventi");
        return getPersistentView(inflater, container, savedInstanceState, R.layout.fragment_interventi);

    }

    public View getPersistentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, int layout){
        if(rootView == null)
            rootView = inflater.inflate(layout, null);
        else ((ViewGroup)rootView).removeView(rootView);
        return rootView;
    }

    public void caricaInterventi(String id)
    {
        String url = "https://pcivile1.altervista.org/RESTAPI/getInterventi.php";
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        Log.i("ID_REQUEST", id);
        JSONObject parameters = new JSONObject(params);
        JSONArray par = new JSONArray();
        par.put(parameters);

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.POST, url, par, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                adapter.clear();
                try {
                    for(int i = response.length()-1; i >= 0; i--)
                    {
                        JSONObject ogg = response.getJSONObject(i);
                        final EmergenzaList line;
                        line = new EmergenzaList();

                        line.data = ogg.getString("Tipo") + "\n" + ogg.getString("Localita") + ", " + ogg.getString("Data");
                        line.id = ogg.getString("ID");
                        adapter.add(line);
                    }
                    adapter.notifyDataSetChanged();
                    ListView l = Objects.requireNonNull(getActivity()).findViewById(R.id.emergenze_list);

                    Animation animFadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
                    l.startAnimation(animFadeIn);

                    l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent i = new Intent(getActivity(), EmergenzaActivity.class);

                            i.putExtra("ID_Emergenza", Objects.requireNonNull(adapter.getItem(position)).id);
                            Log.i("ITEM ID: ",String.valueOf(adapter.getItem(position)));
                            startActivityForResult(i, 0);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Objects.requireNonNull(getActivity()).getApplicationContext(), "ERRORE", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        Volley.newRequestQueue(Objects.requireNonNull(getActivity()).getApplicationContext()).add(jsonRequest);
    }
}

class EmergenzaList{
    String id;
    String data;

    @NonNull
    @Override
    public String toString() {
        return this.data;
    }
}
